let vids = document.getElementsByTagName('video'),
    vid  = false

let noThumbs = document.createElement('style')
noThumbs.innerHTML = `
.ytp-ce-element.ytp-ce-element-show {
  visibility: hidden;
}
.ytp-pause-overlay {
  visibility: hidden;
}
`
document.head.append(noThumbs)

if (vids.length && vids.length == 1) {
  vid = vids[0]
  setTimeout(function() {
    vid.addEventListener('ratechange', quadrupleIt, { once: true, capture: true }, true)
  }, 100)
}

function quadrupleIt(event) {
  event.preventDefault()
  vid.playbackRate = vid.playbackRate * 4
  setTimeout(function() {
    vid.addEventListener('ratechange', quadrupleIt, { once: true, capture: true }, true)
  }, 100)
}
