For when 2 x playback speed just isn't enough, this extension
quadruples the selected playback speed.

E.g.,   0.25 is actually 1 x speed (normal playback),
      Normal is actually 4 x speed
           2 is actually 8 x speed

This is a bit flaky .. speed may indicate incorrectly when moving
between pages. Event doesn't always fire when you select Normal.
I'll have to come back and fix this up later.

Also hides annoying thumbnails when pausing and at video end.